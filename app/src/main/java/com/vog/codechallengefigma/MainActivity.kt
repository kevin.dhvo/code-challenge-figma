package com.vog.codechallengefigma

import android.graphics.ColorFilter
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter.Companion.tint
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.min
import androidx.compose.ui.unit.sp
import com.vog.codechallengefigma.ui.CustomSwitch
import com.vog.codechallengefigma.ui.SwitchColors
import com.vog.codechallengefigma.ui.SwitchDefaults
import com.vog.codechallengefigma.ui.theme.CodeChallengeFigmaTheme
import com.vog.codechallengefigma.ui.theme.Poppins

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CodeChallengeFigmaTheme {

                var selectedTab by remember {
                    mutableStateOf(BottomNavItem.HOME)
                }

                Scaffold(
                    bottomBar = { BottomNavBar(selectedTab) { selectedTab = it } }
                ) {
                    Box(
                        modifier = Modifier
                            .background(Color(0xFFDCE9EF))
                            .padding(it)
                            .fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        Column(
                            modifier = Modifier.fillMaxHeight(),
                            verticalArrangement = Arrangement.SpaceAround,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text(
                                modifier = Modifier,
                                text = when (selectedTab) {
                                    BottomNavItem.NEW -> "New"
                                    BottomNavItem.HOME -> "Home"
                                    BottomNavItem.CALENDAR -> "Calendar"
                                    BottomNavItem.PROFILE -> "Profile"
                                },
                                style = MaterialTheme.typography.h2,
                                color = Color.Black
                            )
                            if (selectedTab == BottomNavItem.HOME) {
                                FigmaCard(
                                    modifier = Modifier
                                        .padding(horizontal = 20.dp)
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
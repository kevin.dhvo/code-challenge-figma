package com.vog.codechallengefigma

import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import com.vog.codechallengefigma.ui.theme.CodeChallengeFigmaTheme

enum class BottomNavItem(
    @DrawableRes val icon: Int,
    val readableName: String
) {
    NEW(R.drawable.ic_new, "New"),
    HOME(R.drawable.home, "Home"),
    CALENDAR(R.drawable.calendar, "Calendar"),
    PROFILE(R.drawable.profile, "Profile")
}

@Composable
fun BottomNavBar(
    selectedTab: BottomNavItem,
    onTabClick: (BottomNavItem) -> Unit
) {
    val selectedShapeModifierLeft = Modifier
        .shadow(
            3.dp, RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 16.dp,
                bottomEnd = 0.dp
            )
        )
        .background(
            Color(0xFF5BA2C6),
            RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 16.dp,
                bottomEnd = 0.dp
            )
        )
        .height(56.dp)

    val selectedShapeModifierRight = Modifier
        .shadow(
            3.dp, RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 16.dp
            )
        )
        .background(
            Color(0xFF5BA2C6),
            RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 16.dp
            )
        )
        .height(56.dp)

    val selectedShapeModifierCenter = Modifier
        .shadow(
            3.dp, RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 0.dp
            )
        )
        .background(
            Color(0xFF5BA2C6),
            RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 0.dp
            )
        )
        .height(56.dp)

    val unselectedShapeModifierLeft = Modifier
        .shadow(
            3.dp, RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 16.dp,
                bottomEnd = 0.dp
            )
        )
        .background(
            Color(0xFF0B0B0C),
            RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 16.dp,
                bottomEnd = 0.dp
            )
        )
        .height(56.dp)

    val unselectedShapeModifierRight = Modifier
        .shadow(
            3.dp, RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 16.dp
            )
        )
        .background(
            Color(0xFF0B0B0C),
            RoundedCornerShape(
                topStart = 16.dp,
                topEnd = 16.dp,
                bottomStart = 0.dp,
                bottomEnd = 16.dp
            )
        )
        .height(56.dp)

//    Row(
//        modifier = Modifier
//            .padding(horizontal = 24.dp, vertical = 21.dp)
//            .fillMaxWidth()
//    ) {
//        Box(
//            modifier = Modifier
//                .shadow(
//                    3.dp, RoundedCornerShape(
//                        topStart = 16.dp,
//                        topEnd = 16.dp,
//                        bottomStart = 16.dp,
//                        bottomEnd = 0.dp
//                    )
//                )
//                .background(
//                    Color(0xFF5BA2C6),
//                    RoundedCornerShape(
//                        topStart = 16.dp,
//                        topEnd = 16.dp,
//                        bottomStart = 16.dp,
//                        bottomEnd = 0.dp
//                    )
//                )
//                .height(56.dp),
//            contentAlignment = Alignment.Center
//        ) {
//            Row(
//                modifier = Modifier.padding(
//                    vertical = 18.25.dp,
//                    horizontal = 16.75.dp
//                ),
//                verticalAlignment = Alignment.CenterVertically
//            ) {
//                Icon(
//                    modifier = Modifier.size(19.5.dp),
//                    imageVector = Icons.Default.Add,
//                    contentDescription = null
//                )
//                Text(
//                    modifier = Modifier.padding(start = 12.75.dp),
//                    text = "New"
//                )
//            }
//        }


    Box() {
//        Row(
//            modifier = Modifier
//                .padding(horizontal = 24.dp, vertical = 21.dp)
//                .fillMaxWidth()
//        ) {
//
//            Box(
//                modifier = Modifier.weight(1f),
//                contentAlignment = Alignment.Center
//            ) {
//                Row(
//                    modifier = Modifier
//                        .padding(vertical = 16.dp, horizontal = 25.dp)
//                        .fillMaxWidth(),
//                    verticalAlignment = Alignment.CenterVertically,
//                    horizontalArrangement = Arrangement.SpaceBetween
//                ) {
//
//                    BottomNavItem.values().forEach {
//                        val selectedColor by animateColorAsState(
//                            if (selectedTab == it) Color.White else Color(
//                                0xFFB1B1B1
//                            )
//                        )
//
//                        Row(
//                            verticalAlignment = CenterVertically
//                        ) {
//                            Icon(
//                                modifier = Modifier
//                                    .size(24.dp)
//                                    .clickable { onTabClick(it) },
//                                painter = painterResource(id = it.icon),
//                                contentDescription = null,
//                                tint = selectedColor
//                            )
//
//                            if (it == selectedTab) {
//                                Text(
//                                    modifier = Modifier.padding(start = 12.dp),
//                                    text = it.readableName,
//                                    color = selectedColor
//                                )
//                            }
//                        }
//
//                    }
//                }
//            }
//        }

        CustomSubcomposeLayout(
            mainContent = {
                BottomNavItem.values().find { it == selectedTab }?.let {
                    Row(
                        modifier = selectedShapeModifierLeft
                            .padding(horizontal = 16.75.dp),
                        verticalAlignment = CenterVertically
                    ) {
                        Icon(
                            modifier = Modifier
                                .size(24.dp)
                                .clickable { onTabClick(it) },
                            painter = painterResource(id = it.icon),
                            contentDescription = null,
                            tint = Color.White
                        )
                        Text(
                            modifier = Modifier.padding(start = 12.dp),
                            text = it.readableName,
                            color = Color.White
                        )
                    }
                }
            }
        ) { size ->

            CustomSubcomposeLayout(mainContent = {
                // Left Black Continuous Shapes
                BottomNavItem.values().takeWhile { it != selectedTab }.forEach {
                    Row(
                        modifier = unselectedShapeModifierLeft
                            .padding(horizontal = 16.75.dp),
                        verticalAlignment = CenterVertically
                    ) {
                        Icon(
                            modifier = Modifier
                                .size(24.dp)
                                .clickable { onTabClick(it) },
                            painter = painterResource(id = it.icon),
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                }

            }) { leftInnerSize ->

                CustomSubcomposeLayout(mainContent = {
                    // Right Black Continuous Shapes
                    BottomNavItem.values().takeLastWhile { it != selectedTab }.forEach {
                        Row(
                            modifier = unselectedShapeModifierRight
                                .padding(horizontal = 16.75.dp),
                            verticalAlignment = CenterVertically
                        ) {
                            Icon(
                                modifier = Modifier
                                    .size(24.dp)
                                    .clickable { onTabClick(it) },
                                painter = painterResource(id = it.icon),
                                contentDescription = null,
                                tint = Color.White
                            )
                        }
                    }
                }) { rightInnerSize ->
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 24.dp, vertical = 21.dp)
                            .fillMaxWidth()
                    ) {
                        Box(
                            modifier = Modifier.weight(1f),
                            contentAlignment = Alignment.Center
                        ) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.SpaceBetween
                            ) {
                                val unselectedColor = Color(0xFFB1B1B1)

                                val leftSide = BottomNavItem.values().takeWhile { it != selectedTab }
                                val rightSide = BottomNavItem.values().takeLastWhile { it != selectedTab }

                                Log.e("LEFT SIZE:", leftSide.size.toString())
                                Log.e("RIGHT SIZE:", rightSide.size.toString())

                                val leftWeightModifier = if (leftSide.size >= rightSide.size) Modifier.weight(1f) else Modifier
                                val rightWeightModifier = if (rightSide.size >= leftSide.size) Modifier.weight(1f) else Modifier

                                // Left Side
                                Box(
                                    modifier = leftWeightModifier
                                        .animateContentSize()
                                        .padding(end = 11.dp)
                                        .size(leftInnerSize)
                                        .shadow(
                                            3.dp, RoundedCornerShape(
                                                topStart = 16.dp,
                                                topEnd = 16.dp,
                                                bottomStart = 16.dp,
                                                bottomEnd = 0.dp
                                            )
                                        )
                                        .background(
                                            Color(0xFF0B0B0C),
                                            RoundedCornerShape(
                                                topStart = 16.dp,
                                                topEnd = 16.dp,
                                                bottomStart = 16.dp,
                                                bottomEnd = 0.dp
                                            )
                                        ),
                                    contentAlignment = Center
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .padding(horizontal = 16.75.dp)
                                            .fillMaxWidth(),
                                        verticalAlignment = CenterVertically,
                                        horizontalArrangement = Arrangement.SpaceBetween
                                    ) {
                                        leftSide.forEach {
                                            Icon(
                                                modifier = Modifier
                                                    .size(24.dp)
                                                    .clickable { onTabClick(it) },
                                                painter = painterResource(id = it.icon),
                                                contentDescription = null,
                                                tint = unselectedColor
                                            )
                                        }
                                    }
                                }

                                // Selected Item
                                val selectedModifier = if (leftSide.isNotEmpty() && rightSide.isNotEmpty()) {
                                    selectedShapeModifierCenter
                                } else if (leftSide.size > rightSide.size) {
                                    selectedShapeModifierRight
                                } else {
                                    selectedShapeModifierLeft
                                }
                                Row(
                                    modifier = selectedModifier
                                        .animateContentSize()
                                        .size(size),
                                    verticalAlignment = CenterVertically,
                                    horizontalArrangement = Arrangement.Center
                                ) {
                                    Icon(
                                        modifier = Modifier
                                            .size(24.dp)
                                            .clickable { onTabClick(selectedTab) },
                                        painter = painterResource(id = selectedTab.icon),
                                        contentDescription = null,
                                        tint = Color.White
                                    )
                                    Text(
                                        modifier = Modifier.padding(start = 12.dp),
                                        text = selectedTab.readableName,
                                        color = Color.White
                                    )
                                }

                                // Right Side
                                Box(
                                    modifier = rightWeightModifier
                                        .animateContentSize()
                                        .padding(start = 11.dp)
                                        .size(rightInnerSize)
                                        .shadow(
                                            3.dp, RoundedCornerShape(
                                                topStart = 16.dp,
                                                topEnd = 16.dp,
                                                bottomStart = 0.dp,
                                                bottomEnd = 16.dp
                                            )
                                        )
                                        .background(
                                            Color(0xFF0B0B0C),
                                            RoundedCornerShape(
                                                topStart = 16.dp,
                                                topEnd = 16.dp,
                                                bottomStart = 0.dp,
                                                bottomEnd = 16.dp
                                            )
                                        ),
                                    contentAlignment = Center
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .padding(horizontal = 16.75.dp)
                                            .fillMaxWidth(),
                                        verticalAlignment = CenterVertically,
                                        horizontalArrangement = Arrangement.SpaceBetween
                                    ) {
                                        rightSide.forEach {
                                            Icon(
                                                modifier = Modifier
                                                    .size(24.dp)
                                                    .clickable { onTabClick(it) },
                                                painter = painterResource(id = it.icon),
                                                contentDescription = null,
                                                tint = unselectedColor
                                            )
                                        }
                                    }
                                }


                            }
                        }
                    }
                }


            }


        }
    }
}

@Composable
internal fun CustomSubcomposeLayout(
    modifier: Modifier = Modifier,
    mainContent: @Composable () -> Unit,
    dependentContent: @Composable (DpSize) -> Unit
) {

    val density = LocalDensity.current
    SubcomposeLayout(
        modifier = modifier
    ) { constraints: Constraints ->

        // Subcompose(compose only a section) main content and get Placeable
        val mainPlaceable: Placeable? = subcompose(SlotsEnum.Main, mainContent)
            .map {
                it.measure(constraints.copy(minWidth = 0, minHeight = 0))
            }.firstOrNull()


        val dependentPlaceable: Placeable =
            subcompose(SlotsEnum.Dependent) {
                dependentContent(
                    DpSize(
                        density.run { mainPlaceable?.width?.toDp()?:0.dp },
                        density.run { mainPlaceable?.height?.toDp()?:0.dp }
                    )
                )
            }
                .map { measurable: Measurable ->
                    measurable.measure(constraints)
                }.first()


        layout(dependentPlaceable.width, dependentPlaceable.height) {
            dependentPlaceable.place(IntOffset.Zero)
        }
    }
}

/**
 * Enum class for SubcomposeLayouts with main and dependent Composables
 */
enum class SlotsEnum { Main, Dependent }

@Preview(showBackground = true, backgroundColor = 0x808080)
@Composable
fun DefaultPreview() {
    CodeChallengeFigmaTheme() {
        BottomNavBar(BottomNavItem.CALENDAR) {}
    }
}
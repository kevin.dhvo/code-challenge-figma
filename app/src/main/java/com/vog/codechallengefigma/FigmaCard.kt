package com.vog.codechallengefigma

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.vog.codechallengefigma.ui.CustomSwitch
import com.vog.codechallengefigma.ui.SwitchDefaults
import com.vog.codechallengefigma.ui.theme.CodeChallengeFigmaTheme
import com.vog.codechallengefigma.ui.theme.Poppins

@Composable
fun FigmaCard(
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 81.dp),
        shape = RoundedCornerShape(16.dp),
        elevation = 2.dp
    ) {
        Row() {
            Image(
                modifier = Modifier
                    .padding(start = 7.dp)
                    .padding(vertical = 7.dp)
                    .size(67.dp)
                    .clip(RoundedCornerShape(9.dp)),
                painter = painterResource(id = R.drawable.yoga),
                contentDescription = null,
                //colorFilter = tint(Color.Unspecified),
                contentScale = ContentScale.FillBounds
            )
            Column(
                modifier = Modifier
                    .padding(start = 12.dp)
                    .wrapContentHeight()
                    .heightIn(min = 81.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Box(
                    modifier = Modifier
                        .padding(top = 7.dp)
                        .heightIn(min = 32.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        modifier = Modifier,
                        text = "Yoga",
                        style = MaterialTheme.typography.body1.copy(
                            fontWeight = FontWeight(500),
                            fontSize = 16.sp,
                            fontFamily = Poppins
                        ),
                        lineHeight = 32.sp,
                        letterSpacing = (-0.41).sp
                    )
                }
                Box(
                    modifier = Modifier.heightIn(min = 32.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        modifier = Modifier,
                        text = "7:30 AM",
                        style = MaterialTheme.typography.body1.copy(
                            fontWeight = FontWeight(400),
                            fontSize = 12.sp,
                            fontFamily = Poppins
                        ),
                        lineHeight = 32.sp,
                        letterSpacing = (-0.41).sp
                    )
                }

            }

            Spacer(modifier = Modifier.weight(1f))

            var checked by remember { mutableStateOf(true) }
            CustomSwitch(
                modifier = Modifier
                    .padding(end = 14.dp)
                    .align(Alignment.CenterVertically),
                checked = checked,
                onCheckedChange = { checked = it },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = Color.White,
                    checkedTrackColor = Color(0xFF5BA2C6),
                    checkedTrackAlpha = 1f,
                    uncheckedTrackColor = Color(0xFFF5F5F5)
                )
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun FigmaCardPreview() {
    CodeChallengeFigmaTheme {
        FigmaCard()
    }
}